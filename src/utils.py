from datetime import datetime, timedelta
from src.models.DateObj import DateObj

def get_week_info() -> DateObj:
    today = datetime.today()
    week_number = today.strftime("%U")
    start_of_week = today - timedelta(days=today.weekday())
    end_of_week = start_of_week + timedelta(days=6)
    previous_week = start_of_week - timedelta(days=6)
    return DateObj(start_of_week.strftime("%Y-%m-%d"), end_of_week.strftime("%Y-%m-%d"), previous_week.strftime("%Y-%m-%d"), int(week_number))