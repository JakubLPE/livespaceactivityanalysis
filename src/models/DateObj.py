from dataclasses import dataclass

@dataclass
class DateObj:
    start_of_week: str
    end_of_week: str
    previous_week: str
    week_number: int