from dataclasses import dataclass

@dataclass(frozen=True)
class AuthorizationData:
    token: str
    session_id: str