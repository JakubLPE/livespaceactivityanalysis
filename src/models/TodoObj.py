from dataclasses import dataclass, fields
from enum import Enum

class TodoObjTypes(Enum):
    PHONE = "ca52eab1-5bd9-8a0b-a950-3b83c3c3b658"
    EMAIL = "609c00b4-d90f-4c9d-d885-ca0c3e0b560a"
    LINKEDIN_INVITE = "c82d5df9-6624-124f-ebf8-a3bb56d2960c"
    LINKEDIN_MESSAGE = "0991a239-e002-8109-d27c-14d178ce1342"
    MEETING = "f71c33c8-c51a-d040-6e82-c0451d626d0b"
    MEETING_PHONE = "d029d30a-304f-b819-8ca5-3096704ed803"
    MEETING_MAIL = "079e0693-396d-136a-38d0-e2f012638640"

class TodoObjStatus(Enum):
    DONE = "0d1fa79b-c8ee-4d12-76cb-9e8ea4e6ccba"
    ACCEPTED = "5c473e9d-a660-3949-c2cf-288868fe561f"
    MISSED = "19de4c94-8398-8654-7de6-cde521d98d11"
    PENDING = "5724b1a3-b48e-13b8-afa1-8a17875d4e6f"

@dataclass(frozen=True)
class TodoObject: 
    id: str
    title: str
    type_id: str
    type_name: str
    status_id: str
    status_name: str
    is_completed: str
    objects: list
    created: str
    modified: str
    creator_name: str
    creator_email: str
    creator_id: str
    
    def is_zaproszenie(self):
        return self.type_id in [TodoObjTypes.EMAIL.value,
                                            TodoObjTypes.LINKEDIN_INVITE.value,
                                            TodoObjTypes.PHONE.value]
    
    def is_korespondencja(self):
        if self.type_id in [TodoObjTypes.EMAIL.value,
                                            TodoObjTypes.LINKEDIN_MESSAGE.value,
                                            TodoObjTypes.PHONE.value]:
            return self.status_id in [TodoObjStatus.DONE.value, TodoObjStatus.ACCEPTED.value]
        return False
    
    def is_spotkanie(self):
        return self.type_id in [TodoObjTypes.MEETING.value,
                                TodoObjTypes.MEETING_PHONE.value,
                                TodoObjTypes.MEETING_MAIL.value
                                ]
    
    def is_creator_email(self, creator_email: str):
        return self.creator_email == creator_email
    
    def is_in_created_date_range(self, data_from: str, date_to: str):
        return data_from <= self.created <= date_to
    
    @classmethod
    def from_json(cls, order:dict):
        return cls(**{key:val for key, val in order.items() if key in [field.name for field in fields(cls)]})