from dataclasses import dataclass, fields

@dataclass(frozen=True)
class WallObject:
    id: int
    text: str
    date: str
    creator_login: str
    object_type: str
    
    def is_szansa(self, date_from: str, date_to: str):
       return 'dodał(a) szansę sprzedaży' in self.text and date_from <= self.date <= date_to
   
    def is_ofertowanie(self, date_from: str, date_to: str):
        return 'na Ofertowanie' in self.text and date_from <= self.date <= date_to
    
    def is_creator_email(self, creator_email: str):
        return self.creator_login == creator_email
    
    @classmethod
    def from_json(cls, order:dict):
        return cls(**{key:val for key, val in order.items() if key in [field.name for field in fields(cls)]})