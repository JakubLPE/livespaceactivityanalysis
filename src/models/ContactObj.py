from dataclasses import dataclass, fields

@dataclass(frozen=True)
class ContactObject:
    id: str
    company_id: str
    name: str
    source_name: str
    owner_email: str
    created: str
    modified: str
    
    def is_creator_email(self, creator_email: str):
        return self.owner_email == creator_email
    
    @classmethod
    def from_json(cls, order:dict):
        return cls(**{key:val for key, val in order.items() if key in [field.name for field in fields(cls)]})   