from src import models

class X:
    
    def __init__(self, todo_objects: list[models.TodoObject], date_from: str, date_to: str):
        self.todo_objects = todo_objects
        self.date_from = date_from
        self.date_to = date_to

    def get_user_action(self):
        invites = 0
        correspondence = 0
        meetings = 0
        for todo in self.todo_objects:
            print(todo)
            if todo.is_in_created_date_range(self.date_from, self.date_to):
                print('accepted')
                if todo.is_zaproszenie():
                    print('invites')
                    invites += 1
                if todo.is_korespondencja():
                    correspondence += 1
                if todo.is_spotkanie():
                    meetings += 1
            else:
                print('NOTNOTNOTN')
        
        return invites, correspondence, meetings
                
        
class Y:
    def __init__(self, wall_object: list[models.WallObject], date_from: str, date_to: str):
        self.wall_object = wall_object        
        self.date_from = date_from
        self.date_to = date_to
    
    def get_user_action(self):
        chance = 0
        ofert = 0
        for wall in self.wall_object:
            if wall.is_szansa(self.date_from, self.date_to):
                chance += 1
            if wall.is_ofertowanie(self.date_from, self.date_to):
                ofert += 1
        
        return chance, ofert

class Z:
    def __init__(self, contact_objs: list[models.ContactObject]):
        self.contact_objs = contact_objs
    
    def get_user_action(self):
        return len(self.contact_objs)
        
class A:
    def __init__(self, date_from: str, date_to: str):
        self.date_from = date_from
        self.date_to = date_to
    
    def process_todo_object(self, objs: models.TodoObject) -> (int, int, int): 
        return X(objs, self.date_from, self.date_to).get_user_action()

    def process_wall_object(self, objs: models.WallObject) -> (int, int):
        return Y(objs, self.date_from, self.date_to).get_user_action()

    def process_contact_object(self, objs: models.ContactObject) -> int:
        return Z(objs).get_user_action()
        



