from pymongo import MongoClient
from pymongo.database import Database
from pymongo.cursor import Cursor
from dataclasses import asdict
from enum import Enum
from src import models
from typing import Optional, Generator

class MongoCollections(Enum):
    CONTACT = 'contact_objects'
    TODO = 'todo_objects'
    WALL = 'wall_objects'


class MongoDB:
    
    CONNECTION_STRING = "mongodb://root:example@mongo:27017"
    
    def __init__(self, dbname: str) -> None:
        self.db: Database = MongoClient(self.CONNECTION_STRING)[dbname]
    
    def delete_collection(self, collection_name: MongoCollections):
        self.db[collection_name.value].drop()

    def insert_many(self, collection_name: MongoCollections, documents: list[dict]):
        self.db[collection_name.value].insert_many(documents)
    
    def insert_one(self, collection_name: MongoCollections, document: dict):
        self.db[collection_name.value].insert_one(document)
    
    def if_exist(self, collection_name: MongoCollections, search_param: dict):
        return self.db[collection_name.value].find_one(search_param)
    
    def find(self, collection_name: MongoCollections, search_param: dict) -> Cursor:
        return self.db[collection_name.value].find(search_param)
    
    def find_doc_by_date(self, collection_name: MongoCollections, date_from: str, date_to: str) -> Cursor:
        search_param={}
        search_param['created']={
            '$gte': date_from,
            '$lte': date_to
        }
        return self.db[collection_name.value].find(search_param)
    
    def find_doc_to_date(self, collection_name: MongoCollections, date_from: str, date_to: str) -> Cursor:
        search_param={}
        search_param['created']={
            '$lte': date_to
        }
        return self.db[collection_name.value].find(search_param)
    
class Repository:
    
    db = MongoDB('livespace')

    def save_none_existing_obj(self, collection_name: MongoCollections, objs: list):
        for obj in [asdict(x) for x in objs]:
            if not self.db.if_exist(collection_name, obj):
                self.db.insert_one(collection_name, obj)
    
    def get_TodoObj(self, date_from: str, date_to: str, user: Optional[str] = 'damian.klimas@lpepoland.com') -> Generator[models.TodoObject, None, None]:
        search_param={}
        search_param['created']={
            '$gte': date_from,
            '$lte': date_to
        }
        if user:
            search_param['creator_email'] = user
        todo_objs = self.db.find(MongoCollections.TODO, search_param)
        for todo in todo_objs:
            del todo['_id']
            yield models.TodoObject(**todo)
            
    def get_WallObj(self, date_from: str, date_to: str, user: Optional[str] = 'damian.klimas@lpepoland.com') -> Generator[models.WallObject, None, None]:
        search_param={}
        search_param['date']={
            '$gte': date_from,
            '$lte': date_to
        }
        if user:
            search_param['creator_login'] = user
        todo_objs = self.db.find(MongoCollections.WALL, search_param)
        for todo in todo_objs:
            del todo['_id']
            yield models.WallObject(**todo)
    
    def get_ContactObj(self,  date_to: str, user: Optional[str] = 'damian.klimas@lpepoland.com') -> Generator[models.ContactObject, None, None]:
        search_param={}
        search_param['created']={
            '$lte': date_to
        }
        if user:
            search_param['owner_email'] = user
        todo_objs = self.db.find(MongoCollections.CONTACT, search_param)
        for todo in todo_objs:
            del todo['_id']
            yield models.ContactObject(**todo)
    
    