from fastapi import FastAPI
from src.LivespaceApi import LivespaceApi, LivespaceAuthorization
from src.configs.config import CONFIG
from src.utils import get_week_info
from src.Analiza import A
from fastapi.encoders import jsonable_encoder
from src.db.mongo_connector import Repository, MongoCollections




date_obj = get_week_info()

# Wywołanie funkcji i pobranie wyników
app = FastAPI()
                            
@app.get("/")
def read_root():
    return {"Hello": "World"}

@app.get("/update_data")
def read_item(date_from: str = date_obj.previous_week, date_to: str = date_obj.end_of_week):
    livespace_auth = LivespaceAuthorization(CONFIG['API_SECRET'], CONFIG['API_KEY'])
    livespace_api= LivespaceApi(livespace_auth)
    todo_objects = livespace_api.getTodoObjects(date_from, date_to)
    wall_objects = livespace_api.getWallObject()
    contact_objects = livespace_api.getContactObject('damian.klimas@lpepoland.com', date_to)
    repo = Repository()
    repo.save_none_existing_obj(MongoCollections.TODO, todo_objects)
    repo.save_none_existing_obj(MongoCollections.WALL, wall_objects)
    repo.save_none_existing_obj(MongoCollections.CONTACT, contact_objects)

@app.get("/get_rates")
def get_rates(date_from: str = date_obj.start_of_week, date_to: str = date_obj.end_of_week):
    db = Repository()
    todo_objects = [x for x in db.get_TodoObj(date_from, date_to)]
    wall_objects = [x for x in db.get_WallObj(date_from, date_to)]
    contact_objects = [x for x in db.get_ContactObj(date_to)]
    transformato = A(date_from, date_to)
    zaproszenia, z_korespondencja,spotkania = transformato.process_todo_object(todo_objects)
    szanse,oferty = transformato.process_wall_object(wall_objects)
    kontakty = transformato.process_contact_object(contact_objects)
    return zaproszenia, z_korespondencja,szanse,spotkania,oferty,kontakty #(count_zaproszenia, count_konwersacje, spotkanie) #jsonable_encoder(response), 
