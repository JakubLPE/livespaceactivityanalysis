import requests
import json
import hashlib
import pandas as pd
from requests.models import Response
from src import models
from enum import Enum



class Url(Enum):
    MAIN = 'https://lpe.livespace.io/api/public/json'
    AUTH = f'{MAIN}/_Api/auth_call/_api_method/getToken'
    TODO = f'{MAIN}/Todo'
    WALL = f'{MAIN}/Wall'
    CONTACT = f'{MAIN}/Contact'


class LivespaceAuthorization:
    
    URL = Url
    
    def __init__(self, api_secret: str, api_key: str):
        self._api_secret = api_secret
        self._api_key = api_key
    
    def start_session(self):
        self._authorization_data = self._get_authorization_data()
        self._header = self._get_header()
        
    def inject_header(self, data: dict) -> dict:
        return {**self._header, **data}
    
    def _get_header(self) -> dict:
        return {
            '_api_auth': 'key',
            '_api_key': self._api_key,
            '_api_sha': self._get_sha(),
            '_api_session': self._authorization_data.session_id
        }
    def _get_authorization_data(self) -> models.AuthorizationData:
        response = self._get_authorization_credential().json()
        if response['result']==200:
            return models.AuthorizationData(**response['data'])

    def _get_authorization_credential(self) -> Response:
        auth_data = {
            '_api_auth': 'key',
            '_api_key': self._api_key
        }
        return requests.post(self.URL.AUTH.value, data=auth_data)
    
    def _get_sha(self):
        data_to_hash = f'{self._api_key}{self._authorization_data.token}{self._api_secret}'.encode('utf-8')
        return hashlib.sha1(data_to_hash).hexdigest()

        

class LivespaceApi:
    
    URL = Url
    
    def __init__(self, livespace_authorization: LivespaceAuthorization):
        self.livespace_authorization = livespace_authorization
        
    def getTodoObjects(self, date_from: str, date_to: str) -> list[models.TodoObject]:
        '''
        $result = $ls->call('Todo/getTodoObjects', array(
            'todo' => array(
                'isCompleted' => 0, // 1 - wykonane, 0 - niewykonane (opcjonalne)
                'types' => array('Spotkanie'), // typ zadania (opcjonalne)
                'datesPeriod' => array( // termin od-do (opcjonalnie)
                    'from' => '2015-10-17', // format Y-m-d H:i:s
                    'to' => '2015-10-18' // format Y-m-d H:i:s
                    ),
                'objects' => array( // powiązane obiekty (opcjonalnie)
                    array(
                    'type' => 'contact', // typ obiektu - contact, company, deal
                    'id' => array( // id obiektu
                        '66c22b04-0879-0c4b-672d-380a93167363',
                        '7be2859b-54a2-ebcb-2024-771148add597'
                        )
                    )
                )
            ) 
        '''
        data = {
            'todo': {
                "datesPeriod": 
                    {
                    'from': f'{date_from}',
                    'to': f'{date_to}'
                    }
                }
            }
        self.livespace_authorization.start_session()
        request_data = self.livespace_authorization.inject_header(data)
        response = self._post(f'{self.URL.TODO.value}/getTodoObjects', request_data).json()
        if response['result'] == 200:
            print(response['data']['todo'][0])
            return [models.TodoObject.from_json(x) for x in response['data']['todo']]
        else:
            assert response['result'] == 200, response
    
    def getWallObject(self) -> list[models.WallObject]:
        '''$result = $ls->call('Wall/getList', array(
            // typ obiektu – contact, company, deal, space (opcjonalnie)
            'object_type' => 'contact',
            // id obiektu (opcjonalnie)
            'object_id' => 'c93ce2dd-aa21-1172-34b0-430af636c674',
            // typ obiektu, do którego jest dodana notatka (opcjonalnie,
            rozdzielone przecinkami)
            'type_name' => 'note,email,activity,phone',
            // data od (opcjonalnie)
            'date_from' => '2015-05-12',
            // data do (opcjonalnie)
            'date_to' => '2016-01-23',
            // wyszukiwana fraza (opcjonalnie)
            'query' => 'tekst',
            'limit' => '100', // Liczba elementów (domyślnie 50)
            'offset' => '0' // Liczba pomijanych początkowych (domyślnie 0)
            ));'''
        data = { 
            "object type": 'deal'
        }
        self.livespace_authorization.start_session()
        request_data = self.livespace_authorization.inject_header(data)
        response = self._post(f'{self.URL.WALL.value}/getList', request_data).json()
        if response['result'] == 200:
            return [models.WallObject.from_json(x) for x in response['data']['items']]
        else:
            assert response['result'] == 200, response
            
    def getContactObject(self, owner_login: str, date_to: str) -> list[models.ContactObject]:
        '''$result = $ls->call('Contact/getAll', array(
            'type' => 'company',
            // dla parametrów podajemy jedną wartość lub kilka oddzielonych przecinkami
            // parametry są opcjonalne ale należy podać przynajmniej jeden warunek
            // nazwa firmy
            'names' => 'Bank XYZ,Firma ABC',
            // NIP (w dowolnym formacie 123-456-78-90, 1234567890, PL123-45-67-890)
            'nip' => '123-456-78-90',
            // REGON
            'regon' => '1234567890123',
            // adres email
            'emails' => 'kontakt@firma.pl',
            // telefon
            'phones' => '666-55-44,333444555',
            // dla parametrów związanych z datą można podać pojedynczą wartość
            (traktowana jest jak wartość 'od') lub tablicę z wartościami 'od' i 'do'
            // data utworzenia
            'created' => array('from' => '2015-01-01 12:00', 'to' => '2015-01-20'),
            // data modyfikacji
            'modified' => '2015-01-15',
            // data ostatniej aktywności
            'last_active' => '2015-01-20',
            // loginy właścicieli rozdzielone przecinkami
            'owner_login' => 'jan.kowalski@firma.com',
            // opcjonalnie sposób porównywania nazwy, nipu i regonu,
            ‘like’ – wzorzec,‘equal’ – dokładne dopasownie, domyślnie ‘like’
            'cond' => 'like',
            // Tagi
            'tags' => 'tag1,tag2',
            // Sposób wyszukiwania po tagach (‘or’ lub ‘and’, domyślnie ‘or’)
            'tags_condition' => 'and',
            'limit' => '1000', // Liczba kontaktów (domyślnie wszystkie)
            'offset' => '0' // Liczba pomijanych początkowych (domyślnie 0)
            ));'''
            
        data = { 
            'type': 'company',
            'owner_login': f'{owner_login}',
            'created':{'to':f'{date_to}'}
            
        }
        self.livespace_authorization.start_session()
        request_data = self.livespace_authorization.inject_header(data)
        response = self._post(f'{self.URL.CONTACT.value}/getAll', request_data).json()
        if response['result'] == 200:
            return [models.ContactObject.from_json(x) for x in response['data']['company']]
        else:
            assert response['result'] == 200, response
            
    @staticmethod
    def _post(url, data):
        return requests.post(url, json=data)

