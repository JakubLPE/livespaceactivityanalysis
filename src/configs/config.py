import json

CONFIG_PATH = "./src/configs/config.json"
CONFIG = json.load(open(file=CONFIG_PATH, encoding='utf-8'))
